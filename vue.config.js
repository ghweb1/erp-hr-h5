// vue.config.js
module.exports = {
  publicPath: './',
  devServer: {
    // 热启动
    open: false,
    // 端口号
    port: 8080,
    // 跳过域名检查
    disableHostCheck: true,
    // 域名代理
    proxy: {
      '/api': {
        target: 'http://88.88.41.200:53901/api',
        // target: 'http://localhost:7001/erp/hr',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      },
    },
  },
  configureWebpack: (config) => {
    config.entry.app = ['babel-polyfill', './src/main.js']
    //删除console插件
    let plugins = [
    ]
  }
}