'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  // 9002
  BASE_API: '"http://10.56.87.171:7001/gh/erp/hr"',
})
