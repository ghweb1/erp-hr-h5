'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
//http://10.56.85.177:7005/ops 8
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  //BASE_API: '"http://localhost:7001/ops"',
  BASE_API: '"http://10.56.87.171:7001/gh/erp/hr"',
})
