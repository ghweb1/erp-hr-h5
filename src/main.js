// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import axios from 'axios'
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
import App from './App'
import router from './router'

/* eslint-disable no-new */
// 显示声明使用VueRouter
Vue.prototype.$ajax = axios
new Vue({
	el: '#app',
	router,
	ElementUI,
	components: { App },
	template: '<App/>'
})

// 防止el-button重复点击
Vue.directive('preventReClick', {
	inserted(el, binding) {
		el.addEventListener('click', () => {
			if (!el.disabled) {
				el.disabled = true;
				el.style.cursur = 'not-allowed'
				setTimeout(() => {
					el.disabled = false
					el.style.cursor = 'pointer'
				}, binding.value || 2000)
			}
		})
	}
});

