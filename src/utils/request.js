import axios from 'axios'
import qs from 'qs'
// import Cookies from 'js-cookie'

// 创建axios实例 此处/api关键如果不填则拿不到服务器中后端api
// const service = axios.create({
//   baseURL: '/api', // api 的 base_url
//   // baseURL: 'http://127.0.0.1:4523/m1/2021768-0-default',
//   timeout: 30000 // 请求超时时间
// })
// axios.defaults.baseURL = 'http://127.0.0.1:4523/m1/2021768-0-default'
// axios.defaults.headers.post['Content-Type'] = 'application/json charset=UTF-8'

// 设置token
axios.interceptors.request.use(
  function (config) {
    let cook = window.location.hash
    // console.log('cook', cook)
    var str = cook.split('=')

    var str2 = str[1]
    let cooks = str2.split('&')
    // //let cooks = cook.split('=')
    let kid = cooks[0]
    sessionStorage.setItem('kid', kid)
    // Cookies.set('kidtoken', cook)
    localStorage.setItem('kidtoken', cook)
    // //let token = Cookies.get('token')
    config.headers['token'] = kid
    return config
  }
)

export default {
  get (url, params = {}) {
    params['t_' + new Date().getTime()] = Math.random()
    return axios.get(url, {
      params: params
    })
  },
  getExcel (url, params = {}) {
    params['t_' + new Date().getTime()] = Math.random()
    return axios({
      url,
      method: 'get',
      params,
      responseType: 'blob'
    })
  },
  post (url, params = {}) {
    if (params.contentType && params.contentType === 'form') {
      return axios.post(url, qs.stringify(params.data), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    } else {
      return axios.post(url, params.data || params)
    }
  },
  postNotQs (url, params = {}) {
    if (params.contentType && params.contentType === 'form') {
      return axios.post(url, params.data, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    } else {
      return axios.post(url, params.data || params)
    }
  },
  postExcel (url, params = {}) {
    return axios({
      url,
      method: 'post',
      data: params.data || params,
      responseType: 'blob'
    })
  },
  put (url, data = {}) {
    return axios.put(url, data)
  },
  delete (url, data = {}) {
    return axios.delete(url, data)
  }
}
