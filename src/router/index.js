import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const routes = [
  {
    name: 'StaffPerfSelect',
    path: '/performance/StaffPerfSelect',
    view: '/performance/StaffPerfSelect',
    meta: { title: '员工绩效查询' },
    component: () =>
      import('@/components/StaffPerfSelect.vue')
  },
  {
    name: 'productDeal',
    path: '/performance/productDeal',
    view: '/performance/productDeal',
    meta: { title: '员工绩效管理表' },
    component: () =>
      import('@/components/productDeal.vue')
  },
  {
    name: 'personnelDeal',
    path: '/performance/personnelDeal',
    view: '/performance/personnelDeal',
    meta: { title: '员工信息查询' },
    component: () =>
      import('@/components/personnelDeal.vue')
  },
  {
    name: 'dimissionDeal',
    path: '/performance/dimissionDeal',
    view: '/performance/dimissionDeal',
    meta: { title: '离职员工查询' },
    component: () =>
      import('@/components/dimissionDeal.vue')
  },
  {
    name: 'approveProcessList',
    path: '/performance/approveProcessList',
    view: '/performance/approveProcessList',
    meta: { title: '待处理任务查询' },
    component: () =>
      import('@/components/approveProcessList.vue')
  },

  {
    name: 'assessmentDetail',
    path: '/performance/assessmentDetail',
    view: '/performance/assessmentDetail',
    meta: { title: '员工考核指标明细' },
    component: () =>
      import('@/components/assessmentDetail.vue')
  },
  {
    name: 'personnelManagement',
    path: '/performance/personnelManagement',
    view: '/performance/personnelManagement',
    meta: { title: '人事绩效管理' },
    component: () =>
      import('@/components/personnelManagement.vue')
  },
  {
    name: 'staffMerits',
    path: '/performance/staffMerits',
    view: '/performance/staffMerits',
    meta: { title: '员工个人绩效查询' },
    component: () =>
      import('@/components/staffMerits.vue')
  },
  {
    name: 'subordinateMerits',
    path: '/performance/subordinateMerits',
    view: '/performance/subordinateMerits',
    meta: { title: '下级绩效查询' },
    component: () =>
      import('@/components/subordinateMerits.vue')
  },
  {
    name: 'meritsExamine',
    path: '/performance/meritsExamine',
    view: '/performance/meritsExamine',
    meta: { title: '部门/机构员工绩效等级审批' },
    component: () =>
      import('@/components/meritsExamine.vue')
  },
  {
    name: 'meritsReCheck',
    path: '/performance/meritsReCheck',
    view: '/performance/meritsReCheck',
    meta: { title: '分公司绩效结果汇总审批' },
    component: () =>
      import('@/components/meritsReCheck.vue')
  },
  {
    name: 'performanceResult',
    path: '/performance/performanceResult',
    view: '/performance/performanceResult',
    meta: { title: '绩效结果查询' },
    component: () =>
      import('@/components/performanceResult.vue')
  },
  {
    name: 'staffManage',
    path: '/staff/staffManage',
    view: '/staff/staffManage',
    meta: { title: '员工信息管理' },
    component: () =>
      import('@/components/staffManage.vue')
  },
  {
    name: 'staffDetail',
    path: '/staff/staffDetail',
    view: '/staff/staffDetail',
    meta: { title: '员工信息详情' },
    component: () =>
      import('@/components/talentStaffDetail.vue')
  }
]

const router = new Router({
  routes
})

router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  // console.log(from)
  // if (from.name === 'weihuDetail') {
  //   var rsCode = sessionStorage.getItem(from.query.kid)
  //   reUser.riskCode = rsCode
  //   console.log('进入了释放用户', reUser)
  //   releaseUer(reUser)
  //   sessionStorage.removeItem(from.query.kid)
  // } else if (to.name === 'productWeihu' && sessionStorage.getItem(to.query.kid) !== null) {
  //   var rsde = sessionStorage.getItem(from.query.kid)
  //   reUser.riskCode = rsde
  //   console.log('进入了释放用户', reUser)
  //   releaseUer(reUser)
  //   sessionStorage.removeItem(from.query.kid)
  // }
  // console.log('从哪个页面', from, '去哪个页面', to)
  // console.log('从哪个页面', from.query.kid, '去哪个页面', to)
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})

export default router
