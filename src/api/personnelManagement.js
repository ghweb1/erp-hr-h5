import request from "../utils/request"

export default {
  // 分页查询人事绩效管理详情
  getList (params) {
    return request.post('api/perfManage/list', params)
  },
  // 导出人事绩效管理明细数据
  exportTarget (params) {
    return request.postExcel('api/perfManage/exportTarget', params)
  },
  // 计算绩效成绩
  calc (params) {
    return request.post('api/perfManage/calc', params)
  },
  // 上级评价公布导入
  importEvaluate (params) {
    return request.post('api/perfManage/importEvaluate', params)
  },
  // 指标结果导入
  importTarget (params) {
    return request.post('api/perfManage/importTarget', params)
  },
  // 绩效导入
  importPerformance (params) {
    return request.post('api/perfManage/importPerformance', params)
  },
  // 发布
  release (params) {
    return request.post('api/perfManage/release', params)
  },
  // 查询审批人集合
  queryAuditGroup (params) {
    return request.get('api/perfManage/queryAuditGroup', params)
  },
  // 发起校正流程
  initiateCheck (params) {
    return request.post('api/perfManage/initiateCheck', params)
  },
  // 提交审批
  initiateAudit (params) {
    return request.post('api/perfManage/initiateAudit', params)
  },
  // 查询员工编号
  queryAuthUser () {
    return request.post('api/auth/user')
  },
  // 员工本人绩效查询
  queryStaffPerf (params) {
    return request.post('api/perfManage/queryStaffPerf', params)
  },
  // 下级员工绩效查询
  queryUnderPerf (params) {
    return request.post('api/perfManage/queryUnderPerf', params)
  },
  // 根据业务号和类型查询流程信息
  queryWorkflowInfo (params) {
    return request.get('api/common/queryWorkflowInfo', params)
  },
  // 分页查询绩效校正列表
  queryPerfCheckList (params) {
    return request.get('api/perfManage/queryPerfCheckList', params)
  },
  // 审批通过
  reviewPass (params) {
    return request.get('api/perfManage/reviewPass', params)
  },
  // 提交绩效校正
  submitPerfCheck (params) {
    return request.get('api/perfManage/submitPerfCheck', params)
  },
  // 发送邮箱
  sendMail (params) {
    return request.post('api/perfManage/sendMail', params)
  }
}
