// 模块化开发：非实时处理数据的api方法
import request from '@/utils/request' // 引入axios实例:它已经配置了baseURL: 8110服务器
// let URL = 'http://127.0.0.1:4523/m1/2021768-0-default'

export default {
  // 1、查询待处理任务
  pendingTaskList (params) {
    return request.post('api/performance/pendingTaskList', params)
  },
  // 根据token查询当前用户
  getUserInfo (params) {
    return request.post('/api/auth/user', params)
  }
}
