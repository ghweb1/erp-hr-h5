import request from "../utils/request"

export default {
  // 分页查询指标明细
  queryTargetList (params) {
    return request.post('api/target/queryTargetList', params)
  },
  // 导出
  exportTarget (params) {
    return request.postExcel('api/target/exportTarget', params)
  },
  // 导入
  importTarget (params) {
    return request.post('api/target/importTarget', params)
  },
  // 查询指标修改轨迹
  queryTargetTrace (params) {
    return request.get('api/target/queryTargetTrace', params)
  },
  // 修改指标明细数据
  updateTarget (params) {
    return request.post('api/target/updateTarget', params)
  },
  // 新增指标明细数据
  saveTarget (params) {
    return request.post('api/target/saveTarget', params)
  },
  // 删除指标明细数据
  deleteTarget (params) {
    return request.post('api/target/deleteTarget', params)
  },
  // 确认指标明细数据
  notarize (params) {
    return request.post('api/target/notarize', params)
  },
  // 指标明细导入模板下载
  download (params) {
    return request.getExcel('api/common/download', params)
  },
  // 机构查询
  queryOrgList (params) {
    return request.get('api/common/queryOrgList', params)
  },
  // 根据机构编码查询部门
  queryDeptByComCode (params) {
    return request.get('api/common/queryDeptByComCode', params)
  }
}