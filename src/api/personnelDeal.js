// 模块化开发：非实时处理数据的api方法
import request from '@/utils/request' // 引入axios实例:它已经配置了baseURL: 8110服务器
// let URL = 'http://127.0.0.1:4523/m1/2021768-0-default'

export default {
  // 1、查询员工信息
  queryStaffList (params) {
    return request.post('/api/entry/list', params)
  },
  // 查询所有机构
  queryOrgList (params) {
    return request.get('/api/common/queryOrgList', params)
  }
}
