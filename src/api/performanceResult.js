import request from "../utils/request"

export default {
  // 分页查询绩效结果
  queryPerformanceResult (params) {
    return request.post('api/perfManage/queryPerfResult', params)
  },
  // 导出绩效结果数据
  exportPerfResult (params) {
    return request.postExcel('api/perfManage/exportPerfResult', params)
  },
}