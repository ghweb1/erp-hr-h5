// 模块化开发：非实时处理数据的api方法
import request from '@/utils/request' // 引入axios实例:它已经配置了baseURL: 8110服务器
// let URL = 'http://127.0.0.1:4523/m1/2021768-0-default'

export default {
  // 分页查询绩效校正列表
  queryPerfCheckList (params) {
    return request.post('/api/perfManage/queryPerfCheckList', params)
  },
  // 根据业务号和类型查询流程信息
  queryWorkflowInfo (params) {
    return request.get('/api/common/queryWorkflowInfo', params)
  },
  // 提交绩效校正
  submitPerfCheck (params) {
    return request.postNotQs('/api/perfManage/submitPerfCheck', params)
  },
  // 审批通过
  reviewPass (params) {
    return request.post('/api/perfManage/reviewPass', params)
  },
  // 审批退回
  reviewBack (params) {
    return request.post('/api/perfManage/reviewBack', params)
  },
  // 查询绩效成绩占比
  queryProportion (params) {
    return request.get('/api/perfManage/queryProportion', params)
  },
  // 导出
  exportPerfCheck (params) {
    return request.postExcel('/api/perfManage/exportPerfCheck', params)
  },
  // 作废
  reviewCancel (params) {
    return request.post('/api/perfManage/reviewCancel', params)
  },
  // 导出分公司绩效页面
  exportPerfReCheck (params) {
    return request.postExcel('/api/perfManage/exportPerfReCheck', params)
  },
  // 查询各机构考核等级分布比例
  queryCompanyGrade (params) {
    return request.get('/api/perfManage/queryCompanyGrade', params)
  },
  // 查询分公司绩效考核分布比例
  queryCheckGrade (params) {
    return request.get('/api/perfManage/queryCheckGrade', params)
  },
}
