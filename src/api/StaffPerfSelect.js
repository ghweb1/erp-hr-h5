// 模块化开发：保全处理数据的api方法
import request from '@/utils/request' // 引入axios实例:它已经配置了baseURL: 8110服务器
// let URL = 'http://10.23.79.96:7001/erp/hr'

export default {
  // 1、查询绩效数据
  queryPerfList (params) {
    return request.post('/api/performance/list', params)
  },
  // 查询所有机构
  queryOrgList (params) {
    return request.get('/api/common/queryOrgList', params)
  },
  // 查询绩效状态枚举
  queryEnumList (params) {
    return request.get('/api/common/enumList', params)
  },
  // 查询绩效状态枚举
  queryDeptByComCode (params) {
    return request.get('/api/common/queryDeptByComCode', params)
  },
  // 发布年度绩效目标与计划制订流程
  initiateYearWork (params) {
    return request.post('/api/performance/initiateYearWork', params)
  },
  // 发布工作绩效考评流程
  initiatePerformance (params) {
    return request.post('/api/performance/initiatePerformance', params)
  },
  // 批量邮件提醒
  remind (params) {
    return request.post('/api/performance/remind', params)
  },
  // 作废
  endFlow (params) {
    return request.post('/api/performance/endFlow', params)
  },
  // 导出
  exportPerf (params) {
    return request.postExcel('/api/performance/exportPerf', params)
  }

}
