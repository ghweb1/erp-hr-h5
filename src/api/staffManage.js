// 模块化开发：非实时处理数据的api方法
import request from '@/utils/request' // 引入axios实例:它已经配置了baseURL: 8110服务器
// let URL = 'http://10.23.79.96:7001/erp/hr'

export default {
  // 员工列表查询
  getListPage (params) {
    return request.post('/api/talent/staff/page', params)
  },
  // 查询下拉框枚举
  getSelectEnums (params) {
    return request.post('/api/talent/staff/enums', params)
  },
  // 查询员工详情
  initInfo (params) {
    return request.post('/api/talent/staff/detail', params)
  },
}
