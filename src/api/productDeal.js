// 模块化开发：非实时处理数据的api方法
import request from '@/utils/request' // 引入axios实例:它已经配置了baseURL: 8110服务器
// let URL = 'http://10.23.79.96:7001/erp/hr'

export default {
  // 查询绩效任务详情
  queryPerfDetail (params) {
    return request.get('/api/performance/performanceDetail', params)
  },
  // 本人提交年度重点工作
  submitYearWork (params) {
    return request.post('/api/performance/submitYearWork', params)
  },
  // 审批通过
  reviewPass (params) {
    return request.post('/api/performance/reviewPass', params)
  },
  // 审批退回
  reviewBack (params) {
    return request.post('/api/performance/reviewBack', params)
  },
  // 查询审批人
  getAuditList (params) {
    return request.get('/api/performance/getAuditList', params)
  },
  // 员工本人绩效查询
  queryStaffPerf (params) {
    return request.post('/api/perfManage/queryStaffPerf', params)
  },
  // 员工本人绩效查询
  queryTargetTrace (params) {
    return request.get('/api/target/queryPersonTarget', params)
  }
}
